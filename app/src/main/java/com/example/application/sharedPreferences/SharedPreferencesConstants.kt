package com.example.application.sharedPreferences

class SharedPreferencesConstants {

    companion object {
        // Session
        const val SESSION = "SESSION"
        const val SESSION_ID = "SESSION_ID"
        const val SESSION_ID_DEFAULT_VALUE = -1

        // Settings
        const val SETTINGS_ACTIVE_NOTIFICATIONS = "SWITCH_ACTIVE_NOTIFICATIONS"
        const val SETTINGS_ACTIVE_NOTIFICATIONS_DEFAULT = true
        const val SETTINGS_ACTIVE_GREETING = "SWITCH_ACTIVE_GREETING"
        const val SETTINGS_ACTIVE_GREETING_DEFAULT = true
    }

}