package com.example.application.sharedPreferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

class SharedPreferencesHelper(context: Context) {
    private var sharedPreferences: SharedPreferences =
        context.getSharedPreferences(SharedPreferencesConstants.SESSION, Context.MODE_PRIVATE)

    private var defaultPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    val hasActiveSession  = { this.getSession() != SharedPreferencesConstants.SESSION_ID_DEFAULT_VALUE }

    val closeSession = {
        this.restartSettings()

        this.saveSession(SharedPreferencesConstants.SESSION_ID_DEFAULT_VALUE)
    }

    fun saveSession(id: Int?) {
        val editor = sharedPreferences.edit()

        if (id != null) {
            editor.putInt(SharedPreferencesConstants.SESSION_ID, id)
        }

        editor.apply()
    }

    private fun restartSettings() {
        val editor = defaultPreferences.edit()

        editor.putBoolean(SharedPreferencesConstants.SETTINGS_ACTIVE_NOTIFICATIONS, SharedPreferencesConstants.SETTINGS_ACTIVE_NOTIFICATIONS_DEFAULT)
        editor.putBoolean(SharedPreferencesConstants.SETTINGS_ACTIVE_GREETING, SharedPreferencesConstants.SETTINGS_ACTIVE_GREETING_DEFAULT)

        editor.apply()
    }

    val getSession = { sharedPreferences.getInt(SharedPreferencesConstants.SESSION_ID, SharedPreferencesConstants.SESSION_ID_DEFAULT_VALUE) }

    val isActiveNotifications = { defaultPreferences.getBoolean(SharedPreferencesConstants.SETTINGS_ACTIVE_NOTIFICATIONS, SharedPreferencesConstants.SETTINGS_ACTIVE_NOTIFICATIONS_DEFAULT) }

    val isActiveGreeting = { defaultPreferences.getBoolean(SharedPreferencesConstants.SETTINGS_ACTIVE_GREETING, SharedPreferencesConstants.SETTINGS_ACTIVE_GREETING_DEFAULT) }
}