package com.example.application.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.application.R
import com.example.application.model.Message

class MessagesAdapter(private val userId: Int, private val listener: OnMessageLongClickListener)  : RecyclerView.Adapter<MessagesAdapter.MessagesViewHolder>() {

    var messages: ArrayList<Message> = ArrayList()

    companion object {
        private const val SENT = 0
        private const val RECEIVED = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesViewHolder {
        val view = when (viewType) {
            SENT -> {
                LayoutInflater.from(parent.context).inflate(R.layout.item_sent, parent, false)
            }
            else -> {
                LayoutInflater.from(parent.context).inflate(R.layout.item_received, parent, false)
            }
        }

        return MessagesViewHolder(view, viewType)
    }

    override fun getItemCount() = messages.size

    override fun onBindViewHolder(holder: MessagesViewHolder, position: Int) {
        holder.txtName.text = messages[position].text
        holder.txtUsername?.text = messages[position].username
        val message = messages[position]
        holder.itemView.setOnLongClickListener{
            listener.onMessageLongClicked(message)

            return@setOnLongClickListener true
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (messages[position].userId == userId) {
            SENT
        } else {
            RECEIVED
        }
    }

    fun addMessage(message: Message) {
        this.messages.add(message)
        this.notifyItemInserted(this.itemCount)
    }

    fun removeMessage(message: Message) {
        val position = messages.indexOf(message)

        this.messages.removeAt(position)
        this.notifyItemRemoved(position)
    }

    fun updateMessage(updateMessage: Message) {
        val position = messages.indexOfFirst { message: Message -> message.id == updateMessage.id }

        this.messages[position] = updateMessage
        this.notifyItemChanged(position)
    }

    class MessagesViewHolder(itemView: View, viewType: Int) : RecyclerView.ViewHolder(itemView) {
        val txtName: TextView = itemView.findViewById(R.id.txtMessage)
        val txtUsername: TextView? = if (viewType != SENT) itemView.findViewById(R.id.txtUsername) else null
    }

    interface OnMessageLongClickListener {
        fun onMessageLongClicked(message: Message)
    }
}