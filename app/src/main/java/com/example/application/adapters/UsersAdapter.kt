package com.example.application.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.application.R
import com.example.application.model.User

class UsersAdapter(private val listener: OnUserClickListener) : RecyclerView.Adapter<UsersAdapter.UsersViewHolder>() {

    var users: List<User> = ArrayList()
    var selectedUsers: ArrayList<User> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersViewHolder {
        return UsersViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_user, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: UsersViewHolder, position: Int) {
        holder.txtName.text = users[position].username
        holder.itemView.setBackgroundColor(if (selectedUsers.contains(users[position])) ContextCompat.getColor(holder.txtName.context, R.color.primaryLight) else ContextCompat.getColor(holder.txtName.context, R.color.design_default_color_background))

        holder.itemView.setOnClickListener {
            val color: Int = if (selectedUsers.contains(users[position])) {
                selectedUsers.remove(users[position])

                ContextCompat.getColor(holder.txtName.context, R.color.design_default_color_background)
            } else {
                selectedUsers.add(users[position])

                ContextCompat.getColor(holder.txtName.context, R.color.primaryLight)
            }

            holder.itemView.setBackgroundColor(color)

            listener.onUserClicked(users[position])
        }
    }

    class UsersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtName: TextView = itemView.findViewById(R.id.txtName)
    }

    interface OnUserClickListener {
        fun onUserClicked(user: User)
    }

}