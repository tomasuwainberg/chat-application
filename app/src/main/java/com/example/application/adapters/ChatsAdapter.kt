package com.example.application.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.application.R
import com.example.application.model.Chat

class ChatsAdapter(private val listener: OnChatClickListener) : RecyclerView.Adapter<ChatsAdapter.ChatsViewHolder>() {

    var chats: List<Chat> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatsViewHolder {
        return ChatsViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_chat, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return chats.size
    }

    override fun onBindViewHolder(holder: ChatsViewHolder, position: Int) {
        holder.txtName.text = chats[position].name

        holder.itemView.setOnClickListener {
            listener.onChatClicked(chats[position])
        }
    }

    class ChatsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtName: TextView = itemView.findViewById(R.id.txtName)
    }

    interface OnChatClickListener {
        fun onChatClicked(chat: Chat)
    }

}