package com.example.application

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class AboutMeActivity : AppCompatActivity() {

    private lateinit var toolbar: Toolbar
    private lateinit var iAnimation: ImageView
    private var clickedAnimationCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_me)


        setupUI()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()

        return true
    }

    private fun setupUI() {
        setupToolbar()

        iAnimation = findViewById(R.id.animation_view)
        iAnimation.setOnClickListener {
            if (clickedAnimationCount == 10) {
                MaterialAlertDialogBuilder(this)
                    .setTitle(getString(R.string.about_me_secret_message_title))
                    .setMessage(getString(R.string.about_me_secret_message_description))
                    .setPositiveButton(getString(R.string.about_me_secret_message_positive_button), null)
                    .show()

                clickedAnimationCount = 0
            } else {
                clickedAnimationCount++
            }
        }
    }

    private fun setupToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = getString(R.string.about_me_title)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
