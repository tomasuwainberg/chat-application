package com.example.application.notifications

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.example.application.ChatActivity
import com.example.application.R
import com.example.application.sharedPreferences.SharedPreferencesHelper

class NotificationsHelper(private var context: Context, private var notificationManager: NotificationManager) {

    private var sharedPreferencesHelper: SharedPreferencesHelper = SharedPreferencesHelper(context)
    private var notificationId: Int = 0
    private var firebaseNotificationId: Int = 1

    companion object {
        const val CHANNEL_ID_DEFAULT = "1"
        const val CHANNEL_NAME_DEFAULT = "All notifications"
        const val CHANNEL_DESCRIPTION_DEFAULT = "Notifications of application"
    }

    init {
        createNotificationChannel()
    }

    private fun createNotificationChannel(id: String = CHANNEL_ID_DEFAULT, name: String = CHANNEL_NAME_DEFAULT, descriptionText: String = CHANNEL_DESCRIPTION_DEFAULT, importance: Int = NotificationManager.IMPORTANCE_DEFAULT) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(id, name, importance).apply {
                description = descriptionText
            }

            // Register the channel with the system
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun showNotification(title: String, contextText: String, channelId: String = CHANNEL_ID_DEFAULT, autoCancel: Boolean = true, pendingIntent: PendingIntent? = null, notificationId: Int = this.notificationId) {
        if (sharedPreferencesHelper.hasActiveSession() && sharedPreferencesHelper.isActiveNotifications()) {
            val notification = NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_chat_black_24dp)
                .setContentTitle(title)
                .setContentText(contextText)
                .setContentIntent(pendingIntent)
                .setAutoCancel(autoCancel)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .build()

            notificationManager.notify(notificationId, notification)
        }
    }

    val clearNotifications = {
        notificationManager.cancel(notificationId)
        notificationManager.cancel(firebaseNotificationId)
    }

    val showCreatedChatNotification = { name: String, chatId: Int ->
        showNotification(
            context.getString(R.string.notification_chat_created_title),
            "${context.getString(R.string.notification_chat_created_prefix_description)} $name",
            pendingIntent = PendingIntent.getActivity(context, 0, ChatActivity.getIntent(context, chatId, name), PendingIntent.FLAG_UPDATE_CURRENT)
        )
    }

    val showFirebaseNotification = { message: String, pendingIntent: PendingIntent ->
        if (sharedPreferencesHelper.hasActiveSession()) {
            showNotification(
                context.getString(R.string.notification_chat_created_title),
                message,
                pendingIntent = pendingIntent,
                notificationId = firebaseNotificationId
            )
        }
    }
}