package com.example.application.db

class DBConstants {

    companion object {
        // Database
        const val DB_NAME = "Chat"
        const val DB_VERSION = 1

        // Users table
        const val TABLE_USERS = "Users"
        const val TABLE_USERS_ID = "Id"
        const val TABLE_USERS_USERNAME = "Username"
        const val TABLE_USERS_PASSWORD = "Password"
        const val TABLE_USERS_CREATION_DATE = "CreationDate"

        // Chats table
        const val TABLE_CHATS = "Chats"
        const val TABLE_CHATS_ID = "Id"
        const val TABLE_CHATS_NAME = "Name"
        const val TABLE_CHATS_CREATION_DATE = "CreationDate"

        // ChatParticipants table
        const val TABLE_CHAT_PARTICIPANTS = "ChatParticipants"
        const val TABLE_CHAT_PARTICIPANTS_ID = "Id"
        const val TABLE_CHAT_PARTICIPANTS_CHAT_ID = "ChatId"
        const val TABLE_CHAT_PARTICIPANTS_USER_ID = "UserId"

        // Messages table
        const val TABLE_MESSAGES = "Messages"
        const val TABLE_MESSAGES_ID = "Id"
        const val TABLE_MESSAGES_TEXT = "Text"
        const val TABLE_MESSAGES_CHAT_ID = "ChatId"
        const val TABLE_MESSAGES_USER_ID = "UserId"
        const val TABLE_MESSAGES_CREATION_DATE = "CreationDate"

        // Insert results
        const val INSERT_RESULT_VIOLATION_CONSTRAINT = -349
        const val INSERT_RESULT_ERROR = -1
    }

}