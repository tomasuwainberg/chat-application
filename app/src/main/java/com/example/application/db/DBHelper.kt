package com.example.application.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.application.model.Chat
import com.example.application.model.Message
import com.example.application.model.User
import com.example.application.utils.HashUtils
import java.sql.Timestamp

class DBHelper(context: Context) :
    SQLiteOpenHelper(context, DBConstants.DB_NAME, null, DBConstants.DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(
            "create table if not exists ${DBConstants.TABLE_USERS} " +
                    "(${DBConstants.TABLE_USERS_ID} integer primary key, " +
                    "${DBConstants.TABLE_USERS_USERNAME} text unique, " +
                    "${DBConstants.TABLE_USERS_PASSWORD} text, " +
                    "${DBConstants.TABLE_USERS_CREATION_DATE} text default current_timestamp)"
        )

        db?.execSQL(
            "create table if not exists ${DBConstants.TABLE_CHATS} " +
                    "(${DBConstants.TABLE_CHATS_ID} integer primary key, " +
                    "${DBConstants.TABLE_CHATS_NAME} text, " +
                    "${DBConstants.TABLE_CHATS_CREATION_DATE} text default current_timestamp)"
        )

        db?.execSQL(
            "create table if not exists ${DBConstants.TABLE_CHAT_PARTICIPANTS} " +
                    "(${DBConstants.TABLE_CHAT_PARTICIPANTS_ID} integer primary key, " +
                    "${DBConstants.TABLE_CHAT_PARTICIPANTS_CHAT_ID} integer, " +
                    "${DBConstants.TABLE_CHAT_PARTICIPANTS_USER_ID} integer, " +
                    "foreign key(${DBConstants.TABLE_CHAT_PARTICIPANTS_CHAT_ID}) references ${DBConstants.TABLE_CHATS}(${DBConstants.TABLE_USERS_ID}), " +
                    "foreign key(${DBConstants.TABLE_CHAT_PARTICIPANTS_USER_ID}) references ${DBConstants.TABLE_USERS}(${DBConstants.TABLE_CHATS_ID}))"
        )

        db?.execSQL(
            "create table if not exists ${DBConstants.TABLE_MESSAGES} " +
                    "(${DBConstants.TABLE_MESSAGES_ID} integer primary key, " +
                    "${DBConstants.TABLE_MESSAGES_TEXT} text, " +
                    "${DBConstants.TABLE_MESSAGES_CHAT_ID} integer, " +
                    "${DBConstants.TABLE_MESSAGES_USER_ID} integer, " +
                    "${DBConstants.TABLE_MESSAGES_CREATION_DATE} text default current_timestamp," +
                    "foreign key(${DBConstants.TABLE_MESSAGES_CHAT_ID}) references ${DBConstants.TABLE_CHATS}(${DBConstants.TABLE_USERS_ID}), " +
                    "foreign key(${DBConstants.TABLE_MESSAGES_USER_ID}) references ${DBConstants.TABLE_USERS}(${DBConstants.TABLE_CHATS_ID}))"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if (oldVersion < newVersion) {
            db?.execSQL("drop table ${DBConstants.TABLE_USERS}")
            onCreate(db)
        }
    }

    fun insertUser(user: User): Int {
        val db = writableDatabase
        val contentValues = ContentValues()
        val hashPassword = HashUtils.sha512(user.password)

        contentValues.put(DBConstants.TABLE_USERS_USERNAME, user.username)
        contentValues.put(DBConstants.TABLE_USERS_PASSWORD, hashPassword)

        return try {
            db.insertOrThrow(DBConstants.TABLE_USERS, null, contentValues).toInt()
        } catch (e: Exception) {
            when (e) {
                is SQLiteConstraintException -> DBConstants.INSERT_RESULT_VIOLATION_CONSTRAINT
                else -> DBConstants.INSERT_RESULT_ERROR
            }
        }
    }

    fun getUser(user: User): User? {
        val db = this.readableDatabase
        val hashPassword = HashUtils.sha512(user.password)
        var userDB: User? = null

        val cur = db.rawQuery(
            "select * from ${DBConstants.TABLE_USERS} where ${DBConstants.TABLE_USERS_USERNAME} = '${user.username}' and ${DBConstants.TABLE_USERS_PASSWORD} = '$hashPassword'", null)

        if (cur.moveToFirst()) {
            userDB = User(
                cur.getInt(cur.getColumnIndex(DBConstants.TABLE_USERS_ID)),
                cur.getString(cur.getColumnIndex(DBConstants.TABLE_USERS_USERNAME)),
                cur.getString(cur.getColumnIndex(DBConstants.TABLE_USERS_PASSWORD)),
                Timestamp.valueOf(cur.getString(cur.getColumnIndex(DBConstants.TABLE_USERS_CREATION_DATE)))
            )
        }

        return userDB
    }

    fun getUserById(userId: Int): User? {
        val db = this.readableDatabase
        var userDB: User? = null

        val cur = db.rawQuery(
            "select * from ${DBConstants.TABLE_USERS} where ${DBConstants.TABLE_USERS_ID} = $userId", null)

        if (cur.moveToFirst()) {
            userDB = User(
                cur.getInt(cur.getColumnIndex(DBConstants.TABLE_USERS_ID)),
                cur.getString(cur.getColumnIndex(DBConstants.TABLE_USERS_USERNAME)),
                cur.getString(cur.getColumnIndex(DBConstants.TABLE_USERS_PASSWORD)),
                Timestamp.valueOf(cur.getString(cur.getColumnIndex(DBConstants.TABLE_USERS_CREATION_DATE)))
            )
        }

        return userDB
    }

    fun getUsers(): List<User> {
        val db = this.readableDatabase

        val cur = db.rawQuery("select * from ${DBConstants.TABLE_USERS}", null)

        return generateSequence { if (cur.moveToNext()) cur else null }
            .map { User(
                it.getInt(it.getColumnIndex(DBConstants.TABLE_USERS_ID)),
                it.getString(it.getColumnIndex(DBConstants.TABLE_USERS_USERNAME)),
                it.getString(it.getColumnIndex(DBConstants.TABLE_USERS_PASSWORD)),
                Timestamp.valueOf(it.getString(it.getColumnIndex(DBConstants.TABLE_USERS_CREATION_DATE))))
            }
            .toList()
    }

    fun insertChat(chat: Chat): Int {
        val db = writableDatabase
        var chatId: Int = DBConstants.INSERT_RESULT_ERROR

        db.beginTransaction()

        try {
            var contentValues = ContentValues()
            contentValues.put(DBConstants.TABLE_CHATS_NAME, chat.name)

            val newChatId = db.insert(DBConstants.TABLE_CHATS, null, contentValues).toInt()

            chat.userIds.forEach {

                contentValues = ContentValues()
                contentValues.put(DBConstants.TABLE_CHAT_PARTICIPANTS_CHAT_ID, newChatId)
                contentValues.put(DBConstants.TABLE_CHAT_PARTICIPANTS_USER_ID, it)

                db.insert(DBConstants.TABLE_CHAT_PARTICIPANTS, null, contentValues)
            }

            db.setTransactionSuccessful()

            chatId = newChatId
        } catch (e: Exception) {
        } finally {
            db.endTransaction()
        }

        return chatId
    }

    fun getChatByUserId(userId: Int): List<Chat> {
        val db = this.readableDatabase
        val cur = db.rawQuery("select * from ${DBConstants.TABLE_CHAT_PARTICIPANTS} where ${DBConstants.TABLE_CHAT_PARTICIPANTS_USER_ID} = $userId", null)

        val concatenatedChatIds: String = generateSequence { if (cur.moveToNext()) cur else null }
            .map { it.getInt(it.getColumnIndex(DBConstants.TABLE_CHAT_PARTICIPANTS_CHAT_ID)) }
            .joinToString { it.toString() }

        cur.close()

        val curChats = db.rawQuery("select * from ${DBConstants.TABLE_CHATS} where ${DBConstants.TABLE_CHATS_ID} in ($concatenatedChatIds)", null)

        return generateSequence { if (curChats.moveToNext()) curChats else null }
            .map { Chat(
                it.getInt(it.getColumnIndex(DBConstants.TABLE_CHATS_ID)),
                it.getString(it.getColumnIndex(DBConstants.TABLE_CHATS_NAME)),
                Timestamp.valueOf(it.getString(it.getColumnIndex(DBConstants.TABLE_CHATS_CREATION_DATE))),
                ArrayList())
            }
            .toList()
    }

    fun insertMessage(message: Message): Int {
        val db = writableDatabase
        val contentValues = ContentValues()

        contentValues.put(DBConstants.TABLE_MESSAGES_TEXT, message.text)
        contentValues.put(DBConstants.TABLE_MESSAGES_CHAT_ID, message.chatId)
        contentValues.put(DBConstants.TABLE_MESSAGES_USER_ID, message.userId)

        return db.insert(DBConstants.TABLE_MESSAGES, null, contentValues).toInt()
    }

    fun getMessagesByChatId(chatId: Int): List<Message> {
        val db = this.readableDatabase
        val messageId = "MESSAGE_ID"
        val cur = db.rawQuery("select M.${DBConstants.TABLE_MESSAGES_ID} as ${messageId},* from ${DBConstants.TABLE_MESSAGES} as M " +
                "join ${DBConstants.TABLE_USERS} as U on U.${DBConstants.TABLE_USERS_ID} = M.${DBConstants.TABLE_MESSAGES_USER_ID} " +
                "where ${DBConstants.TABLE_MESSAGES_CHAT_ID} = $chatId", null)

        return generateSequence { if (cur.moveToNext()) cur else null }
            .map { Message(
                it.getInt(it.getColumnIndex(messageId)),
                it.getString(it.getColumnIndex(DBConstants.TABLE_MESSAGES_TEXT)),
                it.getInt(it.getColumnIndex(DBConstants.TABLE_MESSAGES_CHAT_ID)),
                it.getInt(it.getColumnIndex(DBConstants.TABLE_MESSAGES_USER_ID)),
                Timestamp.valueOf(it.getString(it.getColumnIndex(DBConstants.TABLE_USERS_CREATION_DATE))),
                it.getString(it.getColumnIndex(DBConstants.TABLE_USERS_USERNAME)))
            }
            .toList()
    }

    fun deleteMessage(messageId: Int): Boolean {
        val db = this.writableDatabase
        val result = db.delete(DBConstants.TABLE_MESSAGES, "${DBConstants.TABLE_MESSAGES_ID} = $messageId", null)

        return result > 0
    }

    fun updateMessage(messageId: Int, newText: String): Boolean {
        val db = this.writableDatabase
        val contentValues = ContentValues()

        contentValues.put(DBConstants.TABLE_MESSAGES_TEXT, newText)

        val result = db.update(DBConstants.TABLE_MESSAGES, contentValues, "${DBConstants.TABLE_MESSAGES_ID}=$messageId", null)

        return result > 0
    }
}