package com.example.application

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.application.adapters.ChatsAdapter
import com.example.application.db.DBHelper
import com.example.application.model.Chat
import com.example.application.notifications.NotificationsHelper
import com.example.application.sharedPreferences.SharedPreferencesConstants
import com.example.application.sharedPreferences.SharedPreferencesHelper
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import kotlin.math.absoluteValue

class MainActivity : AppCompatActivity() , ChatsAdapter.OnChatClickListener,
    NavigationView.OnNavigationItemSelectedListener {

    private lateinit var fabAddChat: FloatingActionButton
    private lateinit var toolbar: Toolbar
    private lateinit var rvChats: RecyclerView
    private lateinit var adapter: ChatsAdapter
    private lateinit var txtNotFoundChats: TextView
    private lateinit var sharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var drawer: DrawerLayout
    private lateinit var navigationView: NavigationView
    private lateinit var toggle: ActionBarDrawerToggle
    private var userId: Int =  SharedPreferencesConstants.SESSION_ID_DEFAULT_VALUE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        sharedPreferencesHelper = SharedPreferencesHelper(this)
        userId = sharedPreferencesHelper.getSession()

        setupUI()
        greeting()
    }

    override fun onResume() {
        super.onResume()

        drawer.closeDrawers()
        GetChatByUserId().execute(userId)
    }

    private fun greeting() {
        if(sharedPreferencesHelper.isActiveGreeting()) {
            MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.main_greeting_title))
                .setPositiveButton(getString(R.string.main_greeting_positive_button), null)
                .show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.itemLogOut -> {
                clearNotifications()
                sharedPreferencesHelper.closeSession()

                Intent(this, LoginActivity::class.java).also {
                    startActivity(it)
                    finish()
                }
            }
            else -> {}
        }

        return super.onOptionsItemSelected(item)
    }

    private fun clearNotifications() {
        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        NotificationsHelper(this, notificationManager).clearNotifications()
    }

    private fun setupUI() {
        setupNavigationDrawerAndToolbar()

        txtNotFoundChats = findViewById(R.id.txtNotFoundChats)

        fabAddChat = findViewById(R.id.fabAddChat)
        fabAddChat.setOnClickListener {
            Intent(this, AddChatActivity::class.java).also {
                startActivity(it)
            }
        }

        setupRecyclerView()
    }

    private fun setupNavigationDrawerAndToolbar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawer = findViewById(R.id.drawer_layout)

        toggle = ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer.addDrawerListener(toggle)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        navigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        navigationView.bringToFront()
    }

    private fun setupRecyclerView() {
        rvChats = findViewById(R.id.rvChats)
        rvChats.layoutManager = LinearLayoutManager(this)
        adapter = ChatsAdapter(this)
        rvChats.adapter = adapter
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.nav_settings -> Intent(this, SettingsActivity::class.java).also {
                startActivity(it)
            }
            R.id.nav_about_me -> Intent(this, AboutMeActivity::class.java).also {
                startActivity(it)
            }
            else -> {}
        }

        return true
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        toggle.syncState()
    }

    override fun onChatClicked(chat: Chat) {
        val intent = chat.id?.let { ChatActivity.getIntent(this, it, chat.name) }

        startActivity(intent)
    }

    private fun onAvailableChats(chats: List<Chat>) {
        if (chats.isEmpty()) {
            txtNotFoundChats.visibility = View.VISIBLE
        } else {
            adapter.chats = chats
            adapter.notifyDataSetChanged()
            rvChats.visibility = View.VISIBLE
            txtNotFoundChats.visibility = View.GONE
        }
    }

    inner class GetChatByUserId : AsyncTask<Int, Unit, List<Chat>>() {
        override fun doInBackground(vararg params: Int?): List<Chat>? {
            return params[0]?.absoluteValue?.let { DBHelper(this@MainActivity).getChatByUserId(it) }
        }

        override fun onPostExecute(result: List<Chat>) {
            super.onPostExecute(result)

            this@MainActivity.onAvailableChats(result)
        }
    }
}
