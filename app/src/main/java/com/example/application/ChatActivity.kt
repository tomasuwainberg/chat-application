package com.example.application

import com.example.application.adapters.MessagesAdapter
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.application.db.DBConstants
import com.example.application.db.DBHelper
import com.example.application.model.Message
import com.example.application.model.User
import com.example.application.sharedPreferences.SharedPreferencesHelper
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

class ChatActivity : AppCompatActivity(), MessagesAdapter.OnMessageLongClickListener {

    companion object {
        private const val CHAT_ID = "CHAT_ID"
        private const val CHAT_NAME = "CHAT_NAME"

        fun getIntent(context: Context, chatId: Int, chatName: String): Intent {
            return Intent(context, ChatActivity::class.java).also {
                it.putExtra(CHAT_ID, chatId)
                it.putExtra(CHAT_NAME, chatName)
            }
        }
    }

    private lateinit var rvMessages: RecyclerView
    private lateinit var toolbar: Toolbar
    private lateinit var adapter: MessagesAdapter
    private lateinit var tilMessage: TextInputLayout
    private lateinit var llForm: LinearLayout
    private var newMessage: Message? = null
    private var user: User? = null
    private var chatId: Int = 0
    private var chatName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        val userId = SharedPreferencesHelper(this).getSession()

        chatId = intent.getIntExtra(CHAT_ID, 0)
        chatName = intent.getStringExtra(CHAT_NAME)?.let { it }.toString()

        GetUserAsync().execute(userId)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()

        return true
    }

    override fun onMessageLongClicked(message: Message) {
        if (message.userId == user?.id) {
            MaterialAlertDialogBuilder(this)
                .setTitle(getString(R.string.chat_action_message_title))
                .setPositiveButton(getString(R.string.chat_delete_message)) { _: DialogInterface, _: Int ->
                    DeleteMessageAsync().execute(message)
                }
                .setNegativeButton(getString(R.string.chat_edit_message)) { _: DialogInterface, _:Int ->
                    val inflater = layoutInflater
                    val dialogLayout = inflater.inflate(R.layout.alert_dialog_with_edit_text, null)
                    val editText  = dialogLayout.findViewById<TextInputLayout>(R.id.editText)

                    editText.editText?.setText(message.text)

                    val alertDialog = MaterialAlertDialogBuilder(this)
                        .setTitle(getString(R.string.chat_edit_message))
                        .setPositiveButton(getString(R.string.chat_action_message_save_button)) { _:DialogInterface, _:Int ->
                            val text =  editText.editText?.text.toString()

                            if (text.isNotEmpty()) {
                                message.text = text

                                UpdateMessageAsync().execute(message)
                            }
                        }
                        .setNeutralButton(getString(R.string.chat_action_message_neutral_button), null)

                    alertDialog.setView(dialogLayout)
                    alertDialog.show()
                }
                .setNeutralButton(getString(R.string.chat_action_message_neutral_button), null)
                .show()
        }
    }

    private fun setupUI() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = chatName

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        llForm = findViewById(R.id.llForm)
        llForm.visibility = View.VISIBLE

        setupButtons()
        setupRecyclerView()
    }

    private fun setupButtons() {
        tilMessage = findViewById(R.id.tilMessage)

        tilMessage
            .setEndIconOnClickListener {
                val text = tilMessage.editText?.text.toString()

                if (text.isNotEmpty()) {
                    newMessage = user?.id?.let { it1 ->
                        Message(
                            text = tilMessage.editText?.text.toString(),
                            chatId = chatId,
                            userId = it1,
                            username = user?.username
                        )
                    }

                    tilMessage.editText?.setText(R.string.empty)
                    InsertMessage().execute(newMessage)
                }
            }
    }

    private fun setupRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.stackFromEnd = true

        rvMessages = findViewById(R.id.rvMessages)
        rvMessages.visibility = View.VISIBLE
        rvMessages.layoutManager = linearLayoutManager
        adapter = MessagesAdapter(user?.id!!, this)
        rvMessages.adapter = adapter
    }

    private fun onAvailableMessages(messages: List<Message>) {
        if (messages.isNotEmpty()) {
            adapter.messages = ArrayList(messages)
            adapter.notifyDataSetChanged()
        }
    }

    private fun onAvailableInsertMessageResult(result: Int) {
        if (result != DBConstants.INSERT_RESULT_ERROR) {
            newMessage?.let {
                it.id = result
                adapter.addMessage(it)
            }
            rvMessages.scrollToPosition(adapter.itemCount - 1)
        } else {
            Snackbar.make(findViewById(R.id.csContainer), R.string.chat_insert_message_error, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun onAvailableUser(user: User?) {
        this.user = user

        setupUI()
        GetMessages().execute(chatId)
    }

    private fun onAvailableDeleteMessageResult(result: Boolean, deleteMessage: Message) {
        if (result) {
            adapter.removeMessage(deleteMessage)
            rvMessages.scrollToPosition(adapter.itemCount - 1)
        } else {
            Snackbar.make(findViewById(R.id.csContainer), R.string.chat_delete_message_error, Snackbar.LENGTH_SHORT).show()
        }
    }

    private fun onAvailableUpdateResult(result: Boolean, updateMessage: Message) {
        if (result) {
            adapter.updateMessage(updateMessage)
        } else {
            Snackbar.make(findViewById(R.id.csContainer), R.string.chat_update_message_error, Snackbar.LENGTH_SHORT).show()
        }
    }

    inner class GetMessages : AsyncTask<Int, Unit, List<Message>>() {
        override fun doInBackground(vararg params: Int?): List<Message>? {
            return params[0]?.let { DBHelper(this@ChatActivity).getMessagesByChatId(it) }
        }

        override fun onPostExecute(result: List<Message>) {
            super.onPostExecute(result)

            this@ChatActivity.onAvailableMessages(result)
        }
    }

    inner class InsertMessage : AsyncTask<Message, Unit, Int>() {
        override fun doInBackground(vararg params: Message): Int {
            return DBHelper(this@ChatActivity).insertMessage(params[0])
        }

        override fun onPostExecute(result: Int) {
            super.onPostExecute(result)

            this@ChatActivity.onAvailableInsertMessageResult(result)
        }
    }

    inner class GetUserAsync : AsyncTask<Int, Unit, User?>() {
        override fun doInBackground(vararg params: Int?): User? {
            return params[0]?.let { DBHelper(this@ChatActivity).getUserById(it) }
        }

        override fun onPostExecute(result: User?) {
            super.onPostExecute(result)

            this@ChatActivity.onAvailableUser(result)
        }
    }

    inner class DeleteMessageAsync : AsyncTask<Message, Unit, Boolean>() {
        private lateinit var message: Message

        override fun doInBackground(vararg params: Message?): Boolean? {
            message = params[0]!!

            return message.id?.let { DBHelper(this@ChatActivity).deleteMessage(it) }
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)

            this@ChatActivity.onAvailableDeleteMessageResult(result, message)
        }
    }

    inner class UpdateMessageAsync : AsyncTask<Message, Unit, Boolean>() {
        private lateinit var message: Message

        override fun doInBackground(vararg params: Message?): Boolean? {
            message = params[0]!!

            return DBHelper(this@ChatActivity).updateMessage(message.id!!, message.text)
        }

        override fun onPostExecute(result: Boolean) {
            super.onPostExecute(result)

            this@ChatActivity.onAvailableUpdateResult(result, message)
        }
    }
}
