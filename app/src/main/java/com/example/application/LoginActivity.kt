package com.example.application

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.application.db.DBHelper
import com.example.application.model.User
import com.example.application.sharedPreferences.SharedPreferencesHelper
import com.google.android.material.textfield.TextInputLayout

class LoginActivity : AppCompatActivity() {

    private lateinit var tilUsername : TextInputLayout
    private lateinit var tilPassword : TextInputLayout
    private lateinit var btnSignUp : Button
    private lateinit var btnLogin : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupUI()
    }

    private fun setupUI() {
        tilUsername = findViewById(R.id.tilUsername)
        tilPassword = findViewById(R.id.tilPassword)

        setupButtons()
    }

    private fun setupButtons() {
        btnLogin = findViewById(R.id.btnLogin)
        btnLogin.setOnClickListener {
            validateUser(tilUsername.editText?.text.toString(), tilPassword.editText?.text.toString())
            setupLoading(true)
        }

        btnSignUp = findViewById(R.id.btnSignUp)
        btnSignUp.setOnClickListener {
            Intent(this, RegistryActivity::class.java).also {
                startActivity(it)
                finish()
            }
        }
    }

    private fun setupLoading(isLoading: Boolean) {
        tilUsername.isEnabled = !isLoading
        tilPassword.isEnabled = !isLoading
        btnLogin.isEnabled = !isLoading
        btnSignUp.isEnabled = !isLoading
    }

    private fun finishValidation(user: User?) {
        setupLoading(false)

        if (user != null) {
            saveSession(user.id)
            
            Intent(this, MainActivity::class.java).also {
                startActivity(it)
                finish()
            }
        } else {
            tilUsername.error = getString(R.string.login_username_error)
            tilPassword.error = getString(R.string.login_password_error)
        }
    }

    private fun saveSession(id: Int?) {
        SharedPreferencesHelper(this).saveSession(id)
    }

    private fun validateUser(username: String, password: String) {
        val user = User(username = username, password = password)

        GetUserAsync().execute(user)
    }

    inner class GetUserAsync : AsyncTask<User, Unit, User?>() {
        override fun doInBackground(vararg params: User): User? {
            return DBHelper(this@LoginActivity).getUser(params[0])
        }

        override fun onPostExecute(result: User?) {
            super.onPostExecute(result)

            this@LoginActivity.finishValidation(result)
        }
    }
}
