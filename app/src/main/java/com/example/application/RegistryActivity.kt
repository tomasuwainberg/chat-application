package com.example.application

import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.application.db.DBConstants
import com.example.application.db.DBHelper
import com.example.application.model.User
import com.example.application.sharedPreferences.SharedPreferencesHelper
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

class RegistryActivity : AppCompatActivity() {

    private lateinit var tilUsername : TextInputLayout
    private lateinit var tilPassword : TextInputLayout
    private lateinit var tilConfirmPassword : TextInputLayout
    private lateinit var btnLogin : Button
    private lateinit var btnSignUp : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registry)

        setupUI()
    }

    override fun onBackPressed() {
        super.onBackPressed()

        goToLoginActivity()
    }

    private fun setupUI() {
        tilUsername = findViewById(R.id.tilUsername)
        tilPassword = findViewById(R.id.tilPassword)
        tilConfirmPassword = findViewById(R.id.tilConfirmPassword)

        setupButtons()
    }

    private fun setupButtons() {
        btnSignUp = findViewById(R.id.btnSignUp)
        btnSignUp.setOnClickListener {
            val username = tilUsername.editText?.text.toString()
            val password = tilPassword.editText?.text.toString()

            if (doValidations()) {
                insertUser(username, password)
                setupLoading(true)
            }
        }

        btnLogin = findViewById(R.id.btnLogin)
        btnLogin.setOnClickListener {
            goToLoginActivity()
        }
    }

    private fun doValidations(): Boolean {
        var result = true
        val username = tilUsername.editText?.text.toString()
        val password = tilPassword.editText?.text.toString()
        val confirmPassword = tilConfirmPassword.editText?.text.toString()

        if (username.isEmpty()) {
            tilUsername.error = getString(R.string.registry_username_error)

            result = false
        } else {
            tilUsername.error = getString(R.string.empty)
        }

        if (password.isEmpty()) {
            tilPassword.error = getString(R.string.registry_password_error)

            result = false
        } else {
            tilPassword.error = getString(R.string.empty)
        }

        tilConfirmPassword.error = when {
            confirmPassword.isEmpty() -> {
                result = false

                getString(R.string.registry_password_error)
            }
            password != confirmPassword -> {
                result = false

                getString(R.string.registry_confirm_password_not_equal_to_password_error)
            }
            else -> getString(R.string.empty)
        }

        return result
    }

    private fun setupLoading(isLoading: Boolean) {
        tilUsername.editText?.isEnabled = !isLoading
        tilPassword.editText?.isEnabled = !isLoading
        btnSignUp.isEnabled = !isLoading
    }

    private fun goToLoginActivity() {
        Intent(this, LoginActivity::class.java).also {
            startActivity(it)
            finish()
        }
    }

    private fun onAvailableInsertUserResult(result: Int) {
        setupLoading(false)

        when (result) {
            DBConstants.INSERT_RESULT_VIOLATION_CONSTRAINT -> tilUsername.error = getString(R.string.registry_username_already_register_error)
            DBConstants.INSERT_RESULT_ERROR -> Snackbar.make(findViewById(R.id.clRegistry), R.string.registry_insert_error, Snackbar.LENGTH_SHORT).show()
            else -> Intent(this, MainActivity::class.java).also {
                SharedPreferencesHelper(this).saveSession(result)
                startActivity(it)
                finish()
            }
        }
    }

    private fun insertUser(username: String, password: String) {
        val user = User(username = username, password = password)

        InsertUserAsync().execute(user)
    }

    inner class InsertUserAsync : AsyncTask<User, Unit, Int>() {
        override fun doInBackground(vararg params: User): Int {
            return DBHelper(this@RegistryActivity).insertUser(params[0])
        }

        override fun onPostExecute(result: Int) {
            super.onPostExecute(result)

            this@RegistryActivity.onAvailableInsertUserResult(result)
        }
    }
}
