package com.example.application.model

import java.io.Serializable
import java.util.*

class Message(
    var id: Int? = null,
    var text: String,
    var chatId: Int,
    var userId: Int,
    var creationDate: Date? = null,
    var username: String? = null
): Serializable