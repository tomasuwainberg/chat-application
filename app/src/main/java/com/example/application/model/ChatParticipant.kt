package com.example.application.model

data class ChatParticipant(
    var id: Int? = null,
    var chatId: Int,
    var userId: Int
)