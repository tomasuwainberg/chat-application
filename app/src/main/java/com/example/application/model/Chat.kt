package com.example.application.model

import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

data class Chat(
    var id: Int? = null,
    var name: String,
    var creationDate: Date? = null,
    var userIds: List<Int?> = ArrayList()
) : Serializable