package com.example.application.model

import java.io.Serializable
import java.util.*

data class User(
    var id: Int? = null,
    var username: String,
    var password: String,
    var creationDate: Date? = null
) : Serializable