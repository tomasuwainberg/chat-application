package com.example.application

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.application.sharedPreferences.SharedPreferencesHelper

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        navigateToActivity()
    }

    private fun navigateToActivity() {
        Handler().postDelayed(
            {
                if (SharedPreferencesHelper(this).hasActiveSession()) {
                    Intent(this, MainActivity::class.java).also {
                        startActivity(it)
                    }
                } else {
                    Intent(this, LoginActivity::class.java).also {
                        startActivity(it)
                    }
                }

                finish()
            },
            1500
        )
    }
}
