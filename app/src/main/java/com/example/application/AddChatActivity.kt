package com.example.application

import android.app.NotificationManager
import android.content.Context
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.application.adapters.UsersAdapter
import com.example.application.db.DBConstants
import com.example.application.db.DBHelper
import com.example.application.model.Chat
import com.example.application.model.User
import com.example.application.notifications.NotificationsHelper
import com.example.application.sharedPreferences.SharedPreferencesHelper
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout

class AddChatActivity : AppCompatActivity(), UsersAdapter.OnUserClickListener {

    private lateinit var rvUsers: RecyclerView
    private lateinit var tilName: TextInputLayout
    private lateinit var btnCreate: Button
    private lateinit var toolbar: Toolbar
    private lateinit var adapter: UsersAdapter
    private lateinit var txtParticipants: TextView
    private lateinit var txtNotFoundUsers: TextView
    private lateinit var notificationsHelper: NotificationsHelper
    private var userId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_chat)

        userId = SharedPreferencesHelper(this).getSession()

        setupNotificationManager()
        setupUI()
        GetUsersAsync().execute()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()

        return true
    }


    private fun setupNotificationManager() {
        val notificationManager: NotificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationsHelper = NotificationsHelper(this, notificationManager)
    }

    private fun setupUI() {
        toolbar = findViewById(R.id.toolbar)
        toolbar.title = getString(R.string.add_chat_title)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tilName = findViewById(R.id.tilName)
        txtNotFoundUsers = findViewById(R.id.txtNotFoundUser)
        txtParticipants = findViewById(R.id.txtParticipants)
        txtParticipants.text = getParticipantsTitle(0)

        setupButtons()
        setupRecyclerView()
    }

    private fun setupButtons() {
        btnCreate = findViewById(R.id.btnCreate)
        btnCreate.setOnClickListener {
            if (doValidations()) {
                val users: MutableList<Int?> = adapter.selectedUsers.map { it.id  }.toMutableList()
                users.add(userId)

                InsertChat().execute(Chat(
                    name = tilName.editText?.text.toString(),
                    userIds = users
                ))
            }
        }
    }

    private fun doValidations(): Boolean {
        var result = true
        val chatName = tilName.editText?.text.toString()

        if (chatName.isEmpty()) {
            tilName.error = getString(R.string.add_chat_name_error)

            result = false
        } else {
            tilName.error = getString(R.string.empty)
        }

        if (adapter.selectedUsers.isEmpty()) {
            Snackbar.make(findViewById(R.id.csAddChat), R.string.add_chat_without_participants_error, Snackbar.LENGTH_SHORT).show()

            result = false
        }

        return result
    }

    private fun setupRecyclerView() {
        rvUsers = findViewById(R.id.rvUsers)
        rvUsers.layoutManager = LinearLayoutManager(this)
        adapter = UsersAdapter(this)
        rvUsers.adapter = adapter
    }

    val getParticipantsTitle = { i : Int -> "${getString(R.string.add_chat_title_participants)} ($i)" }

    override fun onUserClicked(user: User) {
        txtParticipants.text = getParticipantsTitle(adapter.selectedUsers.size)
    }

    private fun onAvailableUsers(users: List<User>) {
        val userWithoutCurrent = users.filter { it.id != userId }

        if (userWithoutCurrent.isEmpty()) {
            txtNotFoundUsers.visibility = View.VISIBLE
        } else {
            adapter.users = userWithoutCurrent
            adapter.notifyDataSetChanged()
            rvUsers.visibility = View.VISIBLE
            btnCreate.isEnabled = true
        }
    }

    private fun onAvailableInsertChatResult(chatId: Int) {
        if (chatId == DBConstants.INSERT_RESULT_ERROR) {
            Snackbar.make(findViewById(R.id.clRegistry), R.string.add_chat_insert_error, Snackbar.LENGTH_SHORT).show()
        } else {
            notificationsHelper.showCreatedChatNotification(tilName.editText?.text.toString(), chatId)

            finish()
        }
    }

    inner class GetUsersAsync : AsyncTask<Unit, Unit, List<User>>() {
        override fun doInBackground(vararg params: Unit?): List<User> {
            return DBHelper(this@AddChatActivity).getUsers()
        }

        override fun onPostExecute(result: List<User>) {
            super.onPostExecute(result)

            this@AddChatActivity.onAvailableUsers(result)
        }
    }

    inner class InsertChat : AsyncTask<Chat, Unit, Int>() {
        override fun doInBackground(vararg params: Chat): Int {
            return DBHelper(this@AddChatActivity).insertChat(params[0])
        }

        override fun onPostExecute(result: Int) {
            super.onPostExecute(result)

            this@AddChatActivity.onAvailableInsertChatResult(result)
        }
    }
}
